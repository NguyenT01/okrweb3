import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> listScreen = [];

  @override
  void initState() {
    // TODO: implement initState
    for(int i=0;i<10;i++){
      listScreen.add("Title ${i}");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('OKR'),
      ),
      body: Stack(
        children: [
          Row(
            children: [
              SizedBox(
                height: double.infinity,
                width: 75,
                child: Container(
                  color: Colors.blue,
                  child: ListView.separated(
                    itemCount: listScreen.length,
                    itemBuilder: (context, index){
                      return SizedBox(
                        height: 80,
                        child: Container(
                          color: Colors.yellow,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(Icons.accessibility_new_sharp),
                              const SizedBox(height: 8,),
                              Text(listScreen[index]),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index){
                      return const SizedBox(
                        height: 24,
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
