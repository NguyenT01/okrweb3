import 'package:flutter/material.dart';
import 'export_views.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'KOR Website',
      home: const HomePage(),
    );
  }
}
